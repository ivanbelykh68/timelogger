package ru.belykh.timelogger;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import static junit.framework.TestCase.assertTrue;

/**
 * Unit test for simple TimeLogger.
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TimeLogger{
    private static TimeRowsPool pool = new TimeRowsPool();

    @Test
    public void _0_testTimeRowProducerAddsRowsToPool() {
        TimeRowsProducer producer = new TimeRowsProducer(pool, 1);
        sleep(5500);
        //Hope JVM is no so slow)
        int size = pool.getSize();
        producer.getTimer().cancel();
        sleep(3000);
        assertTrue(pool.getSize() == size && size == 6);
    }

    @Test
    public void _1_testPoolGetsAndRemovesRows() {
        TimeRow timeRow;
        System.out.println(pool.getSize());
        for(int i = 0; i<3; i++){
            timeRow = pool.getNext();
            pool.remove(timeRow);
        }
        assertTrue(pool.getSize() == 3);
    }

    /**
     * stops this thread for N milliseconds
     * @param millis
     */
    private void sleep(long millis){
        try {
            Thread.sleep(millis);
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
