package ru.belykh.timelogger;

import lombok.extern.log4j.Log4j;

/**
 * Consumes TimeRow objects and adds to database using TimeRowDAO in saparate Thread
 */
@Log4j
public class TimeRowsConsumer implements Runnable{

    /**
     * TimeRowDAO to access the database
     */
    private TimeRowDAO timeRowDAO = new TimeRowDAO();

    /**
     * TimeRow pool
     */
    private final TimeRowsPool pool;
    public TimeRowsConsumer(TimeRowsPool pool){
        this.pool = pool;
        new Thread(this).start();
    }

    /**
     * Thread main loop
     */
    @Override
    public void run(){
        while(true){
            TimeRow timeRow = pool.getNext();
            log.info("Got new time row: " + timeRow);
            //slowConnection();
            TimeRow timeRowDB = timeRowDAO.addTimeRow(timeRow);
            if(timeRowDB != null && timeRowDB.getId() != null) {
                log.info("TimeRow successfully added to database: " + timeRowDB);
                pool.remove(timeRow);
            }
            else{
                log.warn("Can't add TimeRow to database: " + timeRow + ". Attempting to add again in 5 seconds!");
                sleep(5);
            }
        }
    }

    /**
     * Slow connection emulation
     */
    private void slowConnection(){
        int maxSeconds = 1;
        sleep(Math.round(Math.random() * maxSeconds));
    }

    /**
     * Stops thread for N seconds
     * @param seconds
     */
    private void sleep(long seconds){
        try { Thread.sleep(1000 * seconds);}
        catch (InterruptedException e) {}
    }

}
