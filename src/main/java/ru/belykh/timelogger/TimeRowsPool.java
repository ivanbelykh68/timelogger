package ru.belykh.timelogger;

import lombok.extern.log4j.Log4j;

import java.util.LinkedList;
import java.util.List;

/**
 * RimeRow pool. Collects TimeRow objects before adding to database
 */
@Log4j
public class TimeRowsPool {
    /**
     * Pool List
     */
    private List<TimeRow> timeRows;

    /**
     * Constructs new TimeRow object with current time
     */
    public TimeRowsPool(){
        timeRows = new LinkedList<TimeRow>();
    }

    /**
     * Adds TimeRow object to pool
     * @param timeRow
     */
    public synchronized void add(TimeRow timeRow){
        timeRows.add(timeRow);
        log.info("Time row added: " + timeRow + ". Total rows: " + getSize());
        notify();
    }

    /**
     * Returns 1-st TimeRow object from pool or waits until it will be added
     * @return
     */
    public synchronized TimeRow getNext(){
        while(timeRows.size() == 0){
            try {
                wait();
            }
            catch (InterruptedException e) {}
        }
        return timeRows.get(0);
    }

    /**
     * Removes TimeRow object fom pool
     * @param timeRow
     */
    public synchronized void remove(TimeRow timeRow){
        timeRows.remove(timeRow);
    }

    /**
     * Size of the pool
     * @return
     */
    public int getSize(){
        return timeRows.size();
    }
}
