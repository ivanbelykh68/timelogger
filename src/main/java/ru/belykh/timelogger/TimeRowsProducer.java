package ru.belykh.timelogger;

import lombok.Getter;
import lombok.extern.log4j.Log4j;

import java.util.Timer;
import java.util.TimerTask;

/**
 * TimeRow producer. Creates new TimeRow objects and adds to pool in separate thread as TimerTask
 */
@Log4j
@Getter
public class TimeRowsProducer extends TimerTask {
    /**
     * TimeRows pool
     */
    private final TimeRowsPool pool;

    /**
     * Timer which runs this producer
     */
    private Timer timer;

    /**
     * Constructor
     * @param pool
     * @param intervalSeconds interval between objects creation
     */
    public TimeRowsProducer(TimeRowsPool pool, int intervalSeconds){
        this.pool = pool;
        (timer = new Timer()).scheduleAtFixedRate(this, 0, 1000 * intervalSeconds);
    }

    /**
     * Creates new TimeRow object and adds to pool
     */
    @Override
    public void run(){
        pool.add(new TimeRow());
    }
}
