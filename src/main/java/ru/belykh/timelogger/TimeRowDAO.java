package ru.belykh.timelogger;

import lombok.extern.log4j.Log4j;

import javax.persistence.EntityTransaction;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * TimeRow Data Access Object
 */
@Log4j
public class TimeRowDAO extends DAO{

    /**
     * Adds new TimeRow entity to database
     * @param timeRow
     * @return saved object with ID
     */
    public TimeRow addTimeRow(TimeRow timeRow){
        log.info("Trying to add TimeRow to database: " + timeRow);
        EntityTransaction transaction = null;
        try {
            transaction = getOpenTransaction();
            TimeRow timeRowFromDB = getEntityManager().merge(timeRow);
            transaction.commit();
            return timeRowFromDB;
        }
        catch(Throwable e){
            connectionProblem(e, transaction);
        }
        return null;
    }

    /**
     * Returns all TimeRow objects from database
     * @return
     */
    public List<TimeRow> getAll(){
        EntityTransaction transaction = null;
        try {
            List<TimeRow> results;
            transaction = getOpenTransaction();
            TypedQuery<TimeRow> namedQuery = getEntityManager().createQuery("SELECT tr FROM TimeRow tr", TimeRow.class);
            results = namedQuery.getResultList();
            transaction.commit();
            return results;
        }
        catch(Throwable e){
            connectionProblem(e, transaction);
        }
        return null;
    }

    /**
     * Removes all TimeRow objects from database
     * @return
     */
    public boolean removeAll(){
        EntityTransaction transaction = null;
        try {
            transaction = getOpenTransaction();
            getEntityManager().createQuery ("DELETE FROM TimeRow").executeUpdate();
            transaction.commit();
        }
        catch(Throwable e){
            connectionProblem(e, transaction);
        }
        return true;
    }
}
