package ru.belykh.timelogger;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.ZonedDateTime;

/**
 * TimeRow database row entiry
 */
@Entity
@Table(name = "time_row")
@Getter @Setter
public class TimeRow {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    /**
     * Time
     */
    private ZonedDateTime time;
    public TimeRow(){
        time = ZonedDateTime.now();
    }

    /**
     * Returns TimeRow entity String view
     * @return
     */
    @Override
    public String toString(){
        if(id == null) return time.toString();
        return time.toString() + " [id=" + id + "]";
    }
}
