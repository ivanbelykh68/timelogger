package ru.belykh.timelogger;

import lombok.extern.log4j.Log4j;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * Base project DAO
 */
@Log4j
public class DAO {
    /**
     * Entiry manager
     */
    private EntityManager em;


    /**
     * Returns EntiryManager
     * can throw Error during the connection
     * @return
     */
    protected EntityManager getEntityManager(){
        if(em == null || !em.isOpen()) em = Persistence.createEntityManagerFactory("TIMELOGGER").createEntityManager();
        return em;
    }

    /**
     * Returns opened EntityTransaction and rollbacks old uncommited transaction if needed
     * @return
     */
    protected EntityTransaction getOpenTransaction(){
        EntityTransaction transaction = getEntityManager().getTransaction();
        // The transaction is still can be opened. Trying to rollback
        if(transaction.isActive()){
            transaction.rollback();
            transaction = getEntityManager().getTransaction();
        }
        transaction.begin();
        return transaction;
    }

    /**
     * Prepare for next connection attempt
     * @param e
     */
    protected void connectionProblem(Throwable e, EntityTransaction transaction){
        //depending on e type we can do different things here
        //resetting EntityManager by default and rollback transaction if needed
        if(transaction != null && transaction.isActive()){
            try{
                transaction.rollback();
            }
            catch(Throwable th){
                log.info("Can't rollback transaction " + th.getMessage());
            }
        }
        em = null;
        log.info("Connection to database failed: " + e.getMessage());
    }

    /**
     * finalizer
     */
    public void closeConnection(){
        if(em == null || !em.isOpen()) return;
        em.close();
    }
}
