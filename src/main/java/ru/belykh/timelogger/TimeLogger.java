package ru.belykh.timelogger;

import lombok.extern.log4j.Log4j;
import org.apache.log4j.BasicConfigurator;

import java.util.List;

@Log4j
public class TimeLogger {
    /**
     * Prints all rows from the database
     */
    private static void printList() {
        TimeRowDAO dao = new TimeRowDAO();
        List<TimeRow> allRows = dao.getAll();
        if (allRows == null) {
            log.warn("Can't get TimeRows from the database");
        } else {
            allRows.forEach(System.out::println);
        }
        dao.closeConnection();
        System.exit(0);
    }

    /**
     * Removes all rows from database
     */
    private static void clean() {
        TimeRowDAO dao = new TimeRowDAO();
        if (!dao.removeAll()) {
            log.warn("Can't remove TimeRows from the database");
        }
        dao.closeConnection();
        System.exit(0);
    }

    /**
     * Prints help message
     */
    private static void help() {
        System.out.println("Options:");
        System.out.println("-p          Display all rows.");
        System.out.println("-r          Remove all rows.");
        System.out.println("Default          Generate rows.");
        System.exit(0);
    }

    /**
     * Runs rows generation
     */
    private static void run(){
        TimeRowsPool pool = new TimeRowsPool();
        new TimeRowsConsumer(pool);
        new TimeRowsProducer(pool, 1);
    }

    /**
     * entry point
     * @param args
     */
    public static void main(String[] args) {
        configureLogger();
        if (args.length > 0 && "-p".equals(args[0])) printList();
        if (args.length > 0 && "-r".equals(args[0])) clean();
        if (args.length > 0) help();
        run();
    }

    /**
     * configures log4j using log4j.properties file
     */
    private static void configureLogger() {
        BasicConfigurator.configure();
    }
}
